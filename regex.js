//
// ^ = start van string; $ = einde van de string
//
const validator = new RegExp('^1{2,4}$')

const demoString = '1111'

console.log(`strng = '${demoString}'`)

// RegExp functies
// console.log('exec  = ', validator.exec(demoString))
console.log('test  = voldoet', validator.test(demoString) ? 'WEL' : 'NIET')

// String functies
console.log('match = voldoet', demoString.match(validator) ? 'WEL' : 'NIET')
